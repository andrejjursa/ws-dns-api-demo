#!/bin/bash

DCOPTS="-f ./docker/docker-compose.yml"
DCRUNOPTS="-u application -w /app"
DCSERVICE="ws_dns_api_server"

docker-compose $DCOPTS up --detach

docker-compose $DCOPTS exec $DCRUNOPTS $DCSERVICE composer install --optimize-autoloader --apcu-autoloader

docker-compose $DCOPTS ps
