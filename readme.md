DNS Settings DEMO
=================

This is a simple application that allows editing DNS settings of single domain using WebSupport Rest API.

To work properly, create a `config.local.php` file inside `config` directory, with the content:

```php
return [
    'apiKey' => 'your-api-key',
    'secret' => 'your-api-key-secret',
    'domain' => 'your-domain',
];
```

You can use the application inside prepared docker service. Just run `./init.sh` from the root directory which
will start the docker service, and the application will be accessible at [`http://localhost:8080/`](http://localhost:8080/).

This project is not using any third party frameworks or libraries, the php composer is used only for building the autoloader.
The php application acts as frontend page service and as an API middleware. It uses custom-made dependency container and
simple router. The whole simple framework, which I have created, is inside `src/Application`. The configuration and bootstrap
is then inside `config` directory.

The user interface allowing DNS editing is written in pure JavaScript. This is again done without any external library.
Whole frontend application is one JavaScript object, which is loaded into page and executed. This part of the application is
whole written in single `.js` file, so, it is a small mess. Also, im more backend developer than frontend, so the code may
not be perfect ... and it is not :). But it seems it all works.

The styling is also done without any styling library. And ... is a little bit simple, but I'm not an artist, so, it is at least
functional :).

All of this takes maybe 2 full man days to create, while the PHP part takes 60% of time, the JavaScript part takes 30% of time,
and the style part the rest ... 10%. Approximately! :)

Hopefully this will fulfill the assignment, I look forward for your evaluation!
