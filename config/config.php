<?php

// THIS IS DEFAULT CONFIGURATION, CREATE config.local.php FILE WHICH WILL RETURN
// CONFIGURATION OVERRIDE, IT WILL BE MERGED WITH array_merge WITH THESE SETTINGS

return [
    'apiURL' => 'https://rest.websupport.sk',
    'apiKey' => '',
    'secret' => '',
    'domain' => '',
    'userId' => 'self',
];
