<?php

use App\DNSDemo\Action\CreateItemAction;
use App\DNSDemo\Action\DeleteItemAction;
use App\DNSDemo\Action\DomainDetailsAction;
use App\DNSDemo\Action\HomepageAction;
use App\DNSDemo\Action\ItemDetailAction;
use App\DNSDemo\Action\ItemListAction;
use App\DNSDemo\Action\UpdateItemAction;
use App\DNSDemo\ComponentFactory\Action\CreateItemActionFactory;
use App\DNSDemo\ComponentFactory\Action\DeleteItemActionFactory;
use App\DNSDemo\ComponentFactory\Action\DomainDetailsActionFactory;
use App\DNSDemo\ComponentFactory\Action\HomepageActionFactory;
use App\DNSDemo\ComponentFactory\Action\ItemDetailActionFactory;
use App\DNSDemo\ComponentFactory\Action\ItemListActionFactory;
use App\DNSDemo\ComponentFactory\Action\UpdateItemActionFactory;
use App\DNSDemo\ComponentFactory\Connector\WSAPIConnectorFactory;
use App\DNSDemo\ComponentFactory\Request\Item\ItemRequestFactoryFactory;
use App\DNSDemo\ComponentFactory\Request\RequestFactoryFactory;
use App\DNSDemo\ComponentFactory\Request\Zone\ZoneRequestFactoryFactory;
use App\DNSDemo\ComponentFactory\Service\ItemServiceFactory;
use App\DNSDemo\ComponentFactory\Service\ZoneServiceFactory;
use App\DNSDemo\Connector\WSAPIConnector;
use App\DNSDemo\Request\Item\ItemRequestFactory;
use App\DNSDemo\Request\RequestFactory;
use App\DNSDemo\Request\Zone\ZoneRequestFactory;
use App\DNSDemo\RequestMapper\RequestMapperFactory;
use App\DNSDemo\ResponseMapper\ResponseMapperFactory;
use App\DNSDemo\Service\ItemService;
use App\DNSDemo\Service\ZoneService;

return [
    'independent' => [
        ResponseMapperFactory::class => ResponseMapperFactory::class,
        RequestMapperFactory::class => RequestMapperFactory::class,
        // CONFIGURATION FILE
        'config' => (static function() {
            $default = require 'config.php';
            if (!file_exists(__DIR__ . '/config.local.php')) {
                return $default;
            }
            return array_merge(
                $default,
                require 'config.local.php'
            );
        })(),
    ],
    'dependent' => [
        WSAPIConnector::class => WSAPIConnectorFactory::class,
        RequestFactory::class => RequestFactoryFactory::class,
        ZoneRequestFactory::class => ZoneRequestFactoryFactory::class,
        ItemRequestFactory::class => ItemRequestFactoryFactory::class,
        ZoneService::class => ZoneServiceFactory::class,
        ItemService::class => ItemServiceFactory::class,
        DomainDetailsAction::class => DomainDetailsActionFactory::class,
        ItemListAction::class => ItemListActionFactory::class,
        ItemDetailAction::class => ItemDetailActionFactory::class,
        CreateItemAction::class => CreateItemActionFactory::class,
        UpdateItemAction::class => UpdateItemActionFactory::class,
        DeleteItemAction::class => DeleteItemActionFactory::class,
        HomepageAction::class => HomepageActionFactory::class,
    ],
];
