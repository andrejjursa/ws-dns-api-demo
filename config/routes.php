<?php

use App\DNSDemo\Action\CreateItemAction;
use App\DNSDemo\Action\DeleteItemAction;
use App\DNSDemo\Action\DomainDetailsAction;
use App\DNSDemo\Action\HomepageAction;
use App\DNSDemo\Action\ItemDetailAction;
use App\DNSDemo\Action\ItemListAction;
use App\DNSDemo\Action\UpdateItemAction;
use App\DNSDemo\Application\Router\Route;

return [
    // API PART
    new Route(['GET'], DomainDetailsAction::class, '/api/v1/zone'),
    new Route(['GET'], ItemListAction::class, '/api/v1/item'),
    new Route(['GET'], ItemDetailAction::class, '/api/v1/item/:id'),
    new Route(['POST'], CreateItemAction::class, '/api/v1/item'),
    new Route(['PUT'], UpdateItemAction::class, '/api/v1/item/:id'),
    new Route(['DELETE'], DeleteItemAction::class, '/api/v1/item/:id'),
    // FRONTPAGE PART
    new Route(['GET'], HomepageAction::class, '/'),
];
