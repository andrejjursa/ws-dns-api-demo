<?php

use App\DNSDemo\Application\Application;
use App\DNSDemo\Application\Configurator\Configurator;
use App\DNSDemo\Application\Container\ContainerManager;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Application\Router\Router;
use App\DNSDemo\Exception\NotFactoryClassException;

require '../vendor/autoload.php';

define('APPLICATION_PATH', rtrim(dirname(__DIR__, 1), '\\/') . DIRECTORY_SEPARATOR);

$containerManager = new ContainerManager();
$configurator = new Configurator($containerManager);

$autoload = require "autoload.php";
$routes = require 'routes.php';

try {
    $configurator->processConfig($autoload);
} catch (NotFactoryClassException $exception) {
    http_response_code(Response::HTTP_INTERNAL_SERVER_ERROR);
    header('Content-Type: text/html');
    echo '<h1>Configuration failed!</h1>';
    
    echo 'Reason: ' . $exception->getMessage() . '<br />';
    
    exit;
}

$router = new Router($routes);
$router->cacheRoutes();

return new Application($containerManager->getContainer(), $router);
