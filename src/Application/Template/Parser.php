<?php

namespace App\DNSDemo\Application\Template;

use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Exception\FileNotFoundException;

class Parser
{
    /**
     * @param string $template
     * @param array<mixed> $param
     * @return string
     */
    public function parse(string $template, array $param): string
    {
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $template;
        if (!file_exists($path)) {
            throw new FileNotFoundException(
                sprintf('Template "%s" does not exists!', $template)
            );
        }
        
        $templateCode = file_get_contents($path);
        
        ob_start();
        eval('?>' . $templateCode);
        return ob_get_clean();
    }
}
