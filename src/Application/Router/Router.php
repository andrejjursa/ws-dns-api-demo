<?php

namespace App\DNSDemo\Application\Router;

use App\DNSDemo\Application\Container\ContainerInterface;

class Router
{
    /**
     * @var array<Route>
     */
    protected array $routes;
    
    /**
     * @var array<string, array<string, Route>>
     */
    protected array $routesCache = [];
    
    /**
     * Router constructor.
     *
     * @param array<Route> $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }
    
    /**
     * Method will prepare multi-level array of routes for implicit access.
     */
    public function cacheRoutes(): void
    {
        $this->routesCache = [];
        /** @var Route $route */
        foreach ($this->routes as $route) {
            foreach ($route->getMethods() as $method) {
                $this->routesCache[strtoupper($method)][$route->getRoute()] = $route;
            }
        }
    }
    
    /**
     * @param string $method
     * @param string $path
     * @return array<mixed>
     */
    public function matchRoute(string $method, string $path): array
    {
        $pathSegments = $this->splitPath($path);
        
        if (!array_key_exists(strtoupper($method), $this->routesCache)) {
            return [ [], null ];
        }
        
        foreach ($this->routesCache[strtoupper($method)] as $routePath => $route) {
            $routePathSegments = $this->splitPath($routePath);
            $parameters = $this->matchRouteSegments($pathSegments, $routePathSegments);
            if (null !== $parameters) {
                return [ $parameters, $route ];
            }
        }
        
        return [ [], null ];
    }
    
    /**
     * @param string $path
     * @return array<string>
     */
    protected function splitPath(string $path): array
    {
        return explode('/', $path);
    }
    
    /**
     * @param array<string> $inputSegments
     * @param array<string> $routeSegments
     * @return array<mixed>|null
     */
    protected function matchRouteSegments(array $inputSegments, array $routeSegments): ?array
    {
        if (count($inputSegments) !== count($routeSegments)) {
            return null;
        }
        
        $output = [];
        
        for ($i=0, $iMax = count($inputSegments); $i < $iMax; ++$i) {
            $routeSegment = $routeSegments[$i];
            $inputSegment = $inputSegments[$i];
            if ($routeSegment[0] === ':') {
                $output[substr($routeSegment, 1)] = $inputSegment;
                continue;
            }
            if ($inputSegment !== $routeSegment) {
                return null;
            }
        }
        
        return $output;
    }
}
