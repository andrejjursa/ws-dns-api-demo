<?php

namespace App\DNSDemo\Application\Router;

use App\DNSDemo\Application\ActionInterface;

class Route
{
    /**
     * @var array<string>
     */
    protected array $methods = [];
    protected string $actionClass;
    protected string $route;
    
    /**
     * Route constructor.
     *
     * @param array $methods
     * @param string $actionClass
     * @param string $route
     */
    public function __construct(array $methods, string $actionClass, string $route)
    {
        $this->methods = $methods;
        $this->actionClass = $actionClass;
        $this->route = $route;
    }
    
    /**
     * @return array<string>
     */
    public function getMethods(): array
    {
        return $this->methods;
    }
    
    /**
     * @return string
     */
    public function getActionClass(): string
    {
        return $this->actionClass;
    }
    
    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }
}
