<?php

namespace App\DNSDemo\Application\Configurator;

use App\DNSDemo\Application\Container\ContainerManager;
use App\DNSDemo\Exception\NotFactoryClassException;

class Configurator
{
    protected ContainerManager $containerManager;
    
    public function __construct(ContainerManager $containerManager)
    {
        $this->containerManager = $containerManager;
    }
    
    /**
     * @param array<string, array<string, mixed>> $config
     * @throws NotFactoryClassException
     */
    public function processConfig(array $config): void
    {
        if (isset($config['independent'])) {
            $this->processIndependent($config['independent']);
        }
        if (isset($config['dependent'])) {
            $this->processDependent($config['dependent']);
        }
    }
    
    /**
     * @param array<string, mixed> $independent
     */
    protected function processIndependent(array $independent): void
    {
        foreach ($independent as $name => $resource) {
            $this->containerManager->addIndependentResource($name, $resource);
        }
    }
    
    /**
     * @param array<string, string> $dependent
     * @throws NotFactoryClassException
     */
    protected function processDependent(array $dependent): void
    {
        foreach ($dependent as $name => $factory) {
            $this->containerManager->addDependentResource($name, $factory);
        }
    }
}
