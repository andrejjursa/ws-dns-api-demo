<?php

namespace App\DNSDemo\Application\HTTP;

use JsonException;

class JsonResponse extends Response
{
    /**
     * JsonResponse constructor.
     *
     * @param array|object $json
     * @param int $httpCode
     * @throws JsonException
     */
    public function __construct(
        $json,
        int $httpCode = self::HTTP_OK
    ) {
        $content = json_encode($json, JSON_THROW_ON_ERROR);
        parent::__construct($content, $httpCode, 'application/json');
    }
}
