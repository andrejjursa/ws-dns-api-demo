<?php

namespace App\DNSDemo\Application\HTTP;

use App\DNSDemo\Application\Template\Parser;

class TemplatedResponse extends Response
{
    /**
     * ParsedResponse constructor.
     *
     * @param string $template
     * @param array<mixed> $data
     * @param int $httpCode
     */
    public function __construct(
        string $template,
        array $data = [],
        int $httpCode = self::HTTP_OK
    )
    {
        $parser = new Parser();
        $content = $parser->parse($template, $data);
        parent::__construct($content, $httpCode, 'text/html');
    }
    
}
