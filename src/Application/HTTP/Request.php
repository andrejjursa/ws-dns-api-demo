<?php

namespace App\DNSDemo\Application\HTTP;

use App\DNSDemo\Application\Router\Route;

class Request
{
    protected string $path;
    
    /**
     * @var array<string, mixed>
     */
    protected array $query;
    
    /**
     * @var array<string, mixed>
     */
    protected array $postData;
    
    protected string $body;
    
    protected string $method;
    
    protected string $contentType;
    
    /**
     * @var array<string, mixed>
     */
    protected array $parameters;
    
    /**
     * @var array<string, mixed>
     */
    protected array $headers;
    
    protected ?Route $route;
    
    /**
     * Request constructor.
     *
     * @param string $path
     * @param array<string, mixed> $query
     * @param array<string, mixed> $postData
     * @param string $body
     * @param string $method
     * @param string $contentType
     * @param array<string, mixed> $parameters
     * @param array<string, mixed> $headers
     * @param Route|null $route
     */
    public function __construct(
        string $path,
        array $query,
        array $postData,
        string $body,
        string $method,
        string $contentType,
        array $parameters,
        array $headers,
        ?Route $route = null
    ) {
        $this->path = $path;
        $this->query = $query;
        $this->postData = $postData;
        $this->body = $body;
        $this->method = $method;
        $this->contentType = $contentType;
        $this->parameters = $parameters;
        $this->headers = $headers;
        $this->route = $route;
    }
    
    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
    
    /**
     * @return array<string, mixed>
     */
    public function getQuery(): array
    {
        return $this->query;
    }
    
    /**
     * @return array<string, mixed>
     */
    public function getPostData(): array
    {
        return $this->postData;
    }
    
    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
    
    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
    
    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }
    
    /**
     * @return array<string, mixed>
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
    
    /**
     * @return array<string, mixed>
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
    
    /**
     * @return Route|null
     */
    public function getRoute(): ?Route
    {
        return $this->route;
    }
}
