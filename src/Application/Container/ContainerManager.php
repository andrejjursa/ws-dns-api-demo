<?php

namespace App\DNSDemo\Application\Container;

use App\DNSDemo\Exception\NotFactoryClassException;

class ContainerManager
{
    /**
     * @var array<string, mixed>
     */
    protected array $independent = [];
    
    /**
     * @var array<string, object>
     */
    protected array $dependent = [];
    
    /**
     * Add independent resource to the container referenced by name. If the resource is class path, then we assume the
     * class has implicit constructor and we create anonymous factory to construct the class in runtime.
     *
     * @param string $resourceName
     * @param $resource
     */
    public function addIndependentResource(string $resourceName, $resource): void
    {
        if (!is_string($resource) || !class_exists($resource)) {
            $this->independent[$resourceName] = $resource;
            return;
        }
        $this->independent[$resourceName] = new class((string)$resource) implements FactoryInterface {
            protected string $resource;
    
            public function __construct(string $resource) {
                $this->resource = $resource;
            }
            
            public function __invoke(ContainerInterface $container)
            {
                return new $this->resource();
            }
        };
    }
    
    /**
     * Add dependent resource to the container referenced by name. The resource has to be class path to the class
     * implementing {@see FactoryInterface}.
     *
     * @param string $resourceName
     * @param string $factoryClass
     * @throws NotFactoryClassException
     */
    public function addDependentResource(string $resourceName, string $factoryClass): void
    {
        if (!class_exists($factoryClass)) {
            throw new NotFactoryClassException(
                sprintf("Class path \"%s\" is not valid class!", $factoryClass)
            );
        }
        
        $factory = new $factoryClass();
        
        if (!$factory instanceof FactoryInterface) {
            throw new NotFactoryClassException(
                sprintf("Class \"%s\" is not instance of %s!", $factoryClass, FactoryInterface::class)
            );
        }
        
        $this->dependent[$resourceName] = $factory;
    }
    
    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return new Container(array_merge($this->independent, $this->dependent));
    }
}
