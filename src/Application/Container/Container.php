<?php

namespace App\DNSDemo\Application\Container;

use App\DNSDemo\Exception\ContainerContentNotSetException;

class Container implements ContainerInterface
{
    /**
     * @var array<string, mixed>
     */
    protected array $content;
    
    /**
     * Container constructor.
     *
     * @param array $content
     */
    public function __construct(array $content)
    {
        $this->content = $content;
    }
    
    /**
     * @inheritdoc
     */
    public function has(string $resource): bool
    {
        return array_key_exists($resource, $this->content);
    }
    
    /**
     * @inheritdoc
     */
    public function get(string $resource)
    {
        if (!$this->has($resource)) {
            throw new ContainerContentNotSetException(
                sprintf("Container does not contains \"%s\"!", $resource)
            );
        }
        
        $content = $this->content[$resource];
        if (is_object($content) && $content instanceof FactoryInterface) {
            return $content($this);
        }
        return $content;
    }
}
