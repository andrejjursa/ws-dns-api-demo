<?php

namespace App\DNSDemo\Application\Container;

use App\DNSDemo\Exception\ContainerContentNotSetException;

interface ContainerInterface
{
    /**
     * @param string $resource
     * @return bool
     */
    public function has(string $resource): bool;
    
    /**
     * @param string $resource
     * @return mixed
     * @throws ContainerContentNotSetException
     */
    public function get(string $resource);
}
