<?php

namespace App\DNSDemo\Application\Container;

use App\DNSDemo\Exception\ContainerContentNotSetException;

interface FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @return mixed
     * @throws ContainerContentNotSetException
     */
    public function __invoke(ContainerInterface $container);
}
