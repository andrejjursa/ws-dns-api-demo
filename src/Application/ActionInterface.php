<?php

namespace App\DNSDemo\Application;

use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;

interface ActionInterface
{
    /**
     * @param Request $request
     * @return Response
     */
    public function handleAction(Request $request): Response;
}
