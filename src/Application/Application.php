<?php

namespace App\DNSDemo\Application;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Application\Router\Router;
use App\DNSDemo\Exception\ContainerContentNotSetException;
use App\DNSDemo\Exception\HttpException;
use App\DNSDemo\Traits\JSONStringToArrayTrait;
use JsonException;
use Throwable;

class Application
{
    use JSONStringToArrayTrait;
    
    protected ContainerInterface $container;
    
    protected Router $router;
    
    /**
     * Application constructor.
     *
     * @param ContainerInterface $container
     * @param Router $router
     */
    public function __construct(ContainerInterface $container, Router $router)
    {
        $this->container = $container;
        $this->router = $router;
    }
    
    /**
     * Run the application.
     */
    public function run(): void
    {
        $request = $this->prepareRequest();
        try {
            $response = $this->executeRequest($request);
        } catch (HttpException $exception) {
            $response = $this->handleException($exception, $exception->getStatusCode());
        } catch (Throwable $exception) {
            $response = $this->handleException($exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $this->presentResponse($response);
    }
    
    /**
     * @return Request
     */
    protected function prepareRequest(): Request
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $urlParts = parse_url($_SERVER['REQUEST_URI']);
        $path = $urlParts['path'];
        $query = $_GET;
        $postData = $_POST;
        $contentType = $_SERVER['CONTENT_TYPE'];
        $body = file_get_contents('php://input');
        $headers = getallheaders();
        [ $parameters, $route ] = $this->router->matchRoute($method, $path);
        
        return new Request($path, $query, $postData, $body, $method, $contentType, $parameters, $headers, $route);
    }
    
    /**
     * @param Throwable $exception
     * @param int $httpStatusCode
     * @return Response
     */
    protected function handleException(
        Throwable $exception,
        int $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR
    ): Response {
        $message = $exception->getMessage();
    
        try {
            $messageToDisplay = $this->jsonStringToArray($message);
        } catch (JsonException $jsonException) {
            $messageToDisplay = $message;
        }
        
        $content = [
            'error' => $messageToDisplay,
            'code' => $exception->getCode(),
        ];
        
        if ($this->readEnv('APPLICATION_MODE') === 'debug') {
            $content['trace'] = $exception->getTrace();
        }
    
        try {
            return new JsonResponse($content, $httpStatusCode);
        } catch (JsonException $e) {
            return new Response(
                'ERROR: ' . $message,
                $httpStatusCode,
                'text/plain'
            );
        }
    }
    
    /**
     * @param Request $request
     * @return Response
     * @throws HttpException
     */
    protected function executeRequest(Request $request): Response
    {
        if ($request->getRoute() === null) {
            throw new HttpException(
                Response::HTTP_METHOD_NOT_ALLOWED,
                sprintf(
                    'Cannot handle request method "%s" at path "%s"!',
                    $request->getMethod(),
                    $request->getPath()
                )
            );
        }
        $class = $request->getRoute()->getActionClass();
        if (!$this->container->has($class)) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                sprintf('Action for path "%s" not found!', $request->getPath())
            );
        }
        try {
            $action = $this->container->get($class);
        } catch (ContainerContentNotSetException $exception) {
            throw new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }
        if (!is_object($action) || !$action instanceof ActionInterface) {
            throw new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                sprintf('Action must be instance of "%s"!', ActionInterface::class)
            );
        }
        return $action->handleAction($request);
    }
    
    /**
     * @param Response $response
     */
    protected function presentResponse(Response $response): void
    {
        http_response_code($response->getHttpCode());
        header('Content-Type: ' . $response->getContentType());
        file_put_contents('php://output', $response->getContent());
    }
    
    /**
     * @param string $variable
     * @return string|null
     */
    protected function readEnv(string $variable): ?string
    {
        $value = getenv($variable, true) ?: getenv($variable);
        if ($value === false) {
            return null;
        }
        return $value;
    }
}
