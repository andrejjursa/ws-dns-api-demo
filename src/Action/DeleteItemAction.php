<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\HttpException;
use App\DNSDemo\Service\ItemService;
use App\DNSDemo\Traits\RequestParameterIdTrait;
use JsonException;

class DeleteItemAction implements ActionInterface
{
    use RequestParameterIdTrait;
    
    protected ItemService $itemService;
    
    /**
     * DeleteItemAction constructor.
     *
     * @param ItemService $itemService
     */
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }
    
    /**
     * @inheritDoc
     * @throws HttpException
     * @throws JsonException
     */
    public function handleAction(Request $request): Response
    {
        $id = $this->getRequestParameterId($request);
        
        $item = $this->itemService->deleteItem($id);
        
        return new JsonResponse($item->toArray());
    }
}
