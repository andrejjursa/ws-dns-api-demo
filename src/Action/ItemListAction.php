<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Service\ItemService;
use JsonException;

class ItemListAction implements ActionInterface
{
    protected ItemService $itemService;
    
    /**
     * ItemListAction constructor.
     *
     * @param ItemService $itemService
     */
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }
    
    /**
     * @inheritDoc
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function handleAction(Request $request): Response
    {
        $itemList = $this->itemService->getItemList();
        return new JsonResponse($itemList->toArray());
    }
}
