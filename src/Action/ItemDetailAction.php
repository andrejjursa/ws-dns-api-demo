<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Exception\HttpException;
use App\DNSDemo\Service\ItemService;
use App\DNSDemo\Traits\RequestParameterIdTrait;
use JsonException;

class ItemDetailAction implements ActionInterface
{
    use RequestParameterIdTrait;
    
    protected ItemService $itemService;
    
    /**
     * ItemDetailAction constructor.
     *
     * @param ItemService $itemService
     */
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }
    
    /**
     * @inheritDoc
     * @throws JsonException
     * @throws ErrorResponseException|HttpException
     */
    public function handleAction(Request $request): Response
    {
        $id = $this->getRequestParameterId($request);
        
        $itemDetail = $this->itemService->getItemDetail($id);
        return new JsonResponse($itemDetail->toArray());
    }
}
