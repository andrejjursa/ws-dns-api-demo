<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Service\ItemService;
use App\DNSDemo\Traits\JSONStringToArrayTrait;
use JsonException;

class CreateItemAction implements ActionInterface
{
    use JSONStringToArrayTrait;
    
    protected ItemService $itemService;
    
    /**
     * CreateItemAction constructor.
     *
     * @param ItemService $itemService
     */
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }
    
    /**
     * @inheritDoc
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function handleAction(Request $request): Response
    {
        $body = $this->jsonStringToArray($request->getBody());
        
        $item = $this->itemService->createItem($body);
        
        return new JsonResponse(
            $item->toArray(),
            $item->getStatus() === 'error' ? Response::HTTP_OK : Response::HTTP_CREATED
        );
    }
}
