<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Application\HTTP\TemplatedResponse;

class HomepageAction implements ActionInterface
{
    protected string $domain;
    
    /**
     * HomepageAction constructor.
     *
     * @param string $domain
     */
    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }
    
    /**
     * @inheritDoc
     */
    public function handleAction(Request $request): Response
    {
        return new TemplatedResponse('homepage.php', [
            'domain' => $this->domain,
        ]);
    }
}
