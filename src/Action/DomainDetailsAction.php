<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Service\ZoneService;
use JsonException;

class DomainDetailsAction implements ActionInterface
{
    protected ZoneService $zoneService;
    
    /**
     * DomainDetailsAction constructor.
     *
     * @param ZoneService $zoneService
     */
    public function __construct(ZoneService $zoneService)
    {
        $this->zoneService = $zoneService;
    }
    
    /**
     * @inheritDoc
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function handleAction(Request $request): Response
    {
        $domainDetails = $this->zoneService->getZoneDetail();
        return new JsonResponse($domainDetails->toArray());
    }
}
