<?php

namespace App\DNSDemo\Action;

use App\DNSDemo\Application\ActionInterface;
use App\DNSDemo\Application\HTTP\JsonResponse;
use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\HttpException;
use App\DNSDemo\Service\ItemService;
use App\DNSDemo\Traits\JSONStringToArrayTrait;
use App\DNSDemo\Traits\RequestParameterIdTrait;
use JsonException;

class UpdateItemAction implements ActionInterface
{
    use RequestParameterIdTrait;
    use JSONStringToArrayTrait;
    
    protected ItemService $itemService;
    
    /**
     * UpdateItemAction constructor.
     *
     * @param ItemService $itemService
     */
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }
    
    /**
     * @inheritDoc
     * @throws JsonException
     * @throws HttpException
     */
    public function handleAction(Request $request): Response
    {
        $id = $this->getRequestParameterId($request);
        $body = $this->jsonStringToArray($request->getBody());
        
        $item = $this->itemService->updateItem($id, $body);
        
        return new JsonResponse($item->toArray());
    }
}
