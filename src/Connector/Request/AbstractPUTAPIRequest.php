<?php

namespace App\DNSDemo\Connector\Request;

abstract class AbstractPUTAPIRequest extends AbstractBaseAPIRequest
{
    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return 'PUT';
    }
}
