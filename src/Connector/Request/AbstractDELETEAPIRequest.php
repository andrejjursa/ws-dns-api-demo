<?php

namespace App\DNSDemo\Connector\Request;

abstract class AbstractDELETEAPIRequest extends AbstractBaseAPIRequest
{
    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return 'DELETE';
    }
}
