<?php

namespace App\DNSDemo\Connector\Request;

abstract class AbstractGETAPIRequest extends AbstractBaseAPIRequest
{
    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return 'GET';
    }
}
