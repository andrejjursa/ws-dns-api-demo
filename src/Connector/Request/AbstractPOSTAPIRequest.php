<?php

namespace App\DNSDemo\Connector\Request;

abstract class AbstractPOSTAPIRequest extends AbstractBaseAPIRequest
{
    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return 'POST';
    }
}
