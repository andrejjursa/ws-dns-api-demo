<?php

namespace App\DNSDemo\Connector\Request;

class APIResponse
{
    protected string $body;
    
    protected int $responseCode;
    
    protected bool $responseError;
    
    /**
     * APIResponse constructor.
     *
     * @param string $body
     * @param int $responseCode
     * @param bool $responseError
     */
    public function __construct(string $body, int $responseCode, bool $responseError)
    {
        $this->body = $body;
        $this->responseCode = $responseCode;
        $this->responseError = $responseError;
    }
    
    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
    
    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }
    
    /**
     * @return bool
     */
    public function isResponseError(): bool
    {
        return $this->responseError;
    }
}
