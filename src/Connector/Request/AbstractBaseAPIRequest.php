<?php

namespace App\DNSDemo\Connector\Request;

abstract class AbstractBaseAPIRequest implements APIRequestInterface
{
    /**
     * @inheritDoc
     */
    public function getData(): ?array
    {
        return null;
    }
    
    /**
     * @inheritDoc
     */
    public function getQuery(): array
    {
        return [];
    }
}
