<?php

namespace App\DNSDemo\Connector\Request;

interface APIRequestInterface
{
    /**
     * Request method.
     *
     * @return string
     */
    public function getMethod(): string;
    
    /**
     * Request body data (will be encoded to JSON).
     *
     * @return array<mixed>|null
     */
    public function getData(): ?array;
    
    /**
     * Request path.
     *
     * @return string
     */
    public function getPath(): string;
    
    /**
     * Request query parameters.
     *
     * @return array<mixed>
     */
    public function getQuery(): array;
}
