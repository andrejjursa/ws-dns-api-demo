<?php

namespace App\DNSDemo\Connector;

use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Connector\Request\APIRequestInterface;
use App\DNSDemo\Connector\Request\APIResponse;
use JsonException;

class WSAPIConnector
{
    protected string $apiUrl;
    
    protected string $apiKey;
    
    protected string $secret;
    
    /**
     * WSAPIConnector constructor.
     *
     * @param string $apiUrl
     * @param string $apiKey
     * @param string $secret
     */
    public function __construct(string $apiUrl, string $apiKey, string $secret)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        $this->secret = $secret;
    }
    
    /**
     * @param APIRequestInterface $APIRequest
     * @return APIResponse
     * @throws JsonException
     */
    public function call(APIRequestInterface $APIRequest): APIResponse
    {
        $time = time();
        $path = $APIRequest->getPath();
        $signature = $this->computeSignature($path, $APIRequest->getMethod(), $time);
        $query = http_build_query($APIRequest->getQuery());
    
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sprintf(
            '%s:%s%s',
            $this->apiUrl,
            $path,
            empty($query) ? '' : '?' . $query
        ));
    
        $headers = [
            'Date: ' . gmdate('Ymd\THis\Z', $time),
            'Accept: application/json',
        ];
        
        $data = $APIRequest->getData();
        if (null !== $data) {
            $headers[] = 'Content-Type: application/json';
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_THROW_ON_ERROR));
        }
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $APIRequest->getMethod());
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->apiKey . ':' . $signature);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        
        $response = curl_exec($curl);
        
        if (false === $response) {
            $responseObject = $this->responseFailed(curl_error($curl));
            curl_close($curl);
            return $responseObject;
        }
        $responseObject = $this->parseResponse($response, curl_getinfo($curl));
        curl_close($curl);
        return $responseObject;
    }
    
    /**
     * @param string $path
     * @param string $method
     * @param int $time
     * @return string
     */
    protected function computeSignature(string $path, string $method, int $time): string
    {
        $canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
        return hash_hmac('sha1', $canonicalRequest, $this->secret);
    }
    
    /**
     * @param string $curlError
     * @return APIResponse
     */
    protected function responseFailed(string $curlError): APIResponse
    {
        return new APIResponse($curlError, Response::HTTP_INTERNAL_SERVER_ERROR, true);
    }
    
    /**
     * @param string $response
     * @param array $curlInfo
     * @return APIResponse
     */
    protected function parseResponse(string $response, array $curlInfo): APIResponse
    {
        $httpCode = (int)$curlInfo['http_code'];
        return new APIResponse($response, $httpCode, $httpCode >= Response::HTTP_BAD_REQUEST);
    }
}
