<?php

namespace App\DNSDemo\Traits;

use JsonException;

trait JSONStringToArrayTrait
{
    /**
     * @param string $jsonString
     * @return array<mixed>
     * @throws JsonException
     */
    protected function jsonStringToArray(string $jsonString): array
    {
        return json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR);
    }
}
