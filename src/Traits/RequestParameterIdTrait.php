<?php

namespace App\DNSDemo\Traits;

use App\DNSDemo\Application\HTTP\Request;
use App\DNSDemo\Application\HTTP\Response;
use App\DNSDemo\Exception\HttpException;

trait RequestParameterIdTrait
{
    /**
     * @param Request $request
     * @return int
     * @throws HttpException
     */
    protected function getRequestParameterId(Request $request): int
    {
        $parameters = $request->getParameters();
        if (!isset($parameters['id']) || !is_numeric($parameters['id'])) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'ID must be set and must be an integer!'
            );
        }
        return (int)$parameters['id'];
    }
}
