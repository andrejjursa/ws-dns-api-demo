<?php

namespace App\DNSDemo\RequestMapper\Item;

use App\DNSDemo\DataObject\Item\Item;
use App\DNSDemo\RequestMapper\RequestMapperInterface;

class ItemRequestMapper implements RequestMapperInterface
{
    /**
     * @inheritDoc
     */
    public function map(array $data): Item
    {
        $item = new Item();
        
        $item->setType($data['type']);
        $item->setName($data['name']);
        $item->setContent($data['content']);
        $item->setTtl($data['ttl']);
        $item->setPriority($data['prio']);
        $item->setWeight($data['weight']);
        $item->setPort($data['port']);
        
        return $item;
    }
}
