<?php

namespace App\DNSDemo\RequestMapper;

use App\DNSDemo\RequestMapper\Item\ItemRequestMapper;

class RequestMapperFactory
{
    /**
     * @return ItemRequestMapper
     */
    public function getItemItemRequestMapper(): ItemRequestMapper
    {
        return new ItemRequestMapper();
    }
}
