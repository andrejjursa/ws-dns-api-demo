<?php

namespace App\DNSDemo\RequestMapper;

interface RequestMapperInterface
{
    /**
     * @param array<mixed> $data
     * @return mixed
     */
    public function map(array $data);
}
