<?php

namespace App\DNSDemo\ResponseMapper\Item;

use App\DNSDemo\DataObject\Item\ItemDetail;
use App\DNSDemo\ResponseMapper\ResponseMapperInterface;
use App\DNSDemo\ResponseMapper\Zone\DomainDetailResponseMapper;

class ItemDetailResponseMapper implements ResponseMapperInterface
{
    protected DomainDetailResponseMapper $domainDetailResponseMapper;
    
    protected SimpleItemResponseMapper $simpleItemResponseMapper;
    
    /**
     * ItemDetailResponseMapper constructor.
     *
     * @param DomainDetailResponseMapper $domainDetailResponseMapper
     * @param SimpleItemResponseMapper $simpleItemResponseMapper
     */
    public function __construct(
        DomainDetailResponseMapper $domainDetailResponseMapper,
        SimpleItemResponseMapper $simpleItemResponseMapper
    ) {
        $this->domainDetailResponseMapper = $domainDetailResponseMapper;
        $this->simpleItemResponseMapper = $simpleItemResponseMapper;
    }
    
    /**
     * @inheritDoc
     */
    public function map(array $data): ItemDetail
    {
        $simpleItem = $this->simpleItemResponseMapper->map($data);
        $itemDetail = new ItemDetail(
            $simpleItem->getId(),
            $simpleItem->getType(),
            $simpleItem->getName(),
            $simpleItem->getContent(),
            $simpleItem->getTtl()
        );
        $itemDetail->setPort($simpleItem->getPort());
        $itemDetail->setWeight($simpleItem->getWeight());
        $itemDetail->setPriority($simpleItem->getPriority());
        $itemDetail->setZone($this->domainDetailResponseMapper->map($data['zone']));
        return $itemDetail;
    }
}
