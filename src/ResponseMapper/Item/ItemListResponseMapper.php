<?php

namespace App\DNSDemo\ResponseMapper\Item;

use App\DNSDemo\DataObject\Item\ItemList;
use App\DNSDemo\ResponseMapper\PagerResponseMapper;
use App\DNSDemo\ResponseMapper\ResponseMapperInterface;

class ItemListResponseMapper implements ResponseMapperInterface
{
    protected SimpleItemResponseMapper $simpleItemResponseMapper;
    
    protected PagerResponseMapper $pagerResponseMapper;
    
    /**
     * ItemListResponseMapper constructor.
     *
     * @param SimpleItemResponseMapper $simpleItemResponseMapper
     * @param PagerResponseMapper $pagerResponseMapper
     */
    public function __construct(
        SimpleItemResponseMapper $simpleItemResponseMapper,
        PagerResponseMapper $pagerResponseMapper
    ) {
        $this->simpleItemResponseMapper = $simpleItemResponseMapper;
        $this->pagerResponseMapper = $pagerResponseMapper;
    }
    
    /**
     * @inheritDoc
     */
    public function map(array $data): ItemList
    {
        $items = array_map(function(array $item) {
            return $this->simpleItemResponseMapper->map($item);
        }, $data['items']);
        return new ItemList($items, $this->pagerResponseMapper->map($data['pager']));
    }
}
