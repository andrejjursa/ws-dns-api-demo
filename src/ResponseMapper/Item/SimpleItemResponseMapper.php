<?php

namespace App\DNSDemo\ResponseMapper\Item;

use App\DNSDemo\DataObject\Item\SimpleItem;
use App\DNSDemo\ResponseMapper\ResponseMapperInterface;

class SimpleItemResponseMapper implements ResponseMapperInterface
{
    /**
     * @inheritDoc
     */
    public function map(array $data): SimpleItem
    {
        $simpleItem = new SimpleItem(
            $data['id'],
            $data['type'],
            $data['name'],
            $data['content'],
            $data['ttl']
        );
        if (isset($data['prio'])) {
            $simpleItem->setPriority($data['prio']);
        }
        if (isset($data['weight'])) {
            $simpleItem->setWeight($data['weight']);
        }
        if (isset($data['port'])) {
            $simpleItem->setPort($data['port']);
        }
        return $simpleItem;
    }
}
