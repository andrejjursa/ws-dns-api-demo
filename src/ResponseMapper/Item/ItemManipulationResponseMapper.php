<?php

namespace App\DNSDemo\ResponseMapper\Item;

use App\DNSDemo\DataObject\Item\ItemManipulation;
use App\DNSDemo\ResponseMapper\ResponseMapperInterface;

class ItemManipulationResponseMapper implements ResponseMapperInterface
{
    protected ItemResultResponseMapper $itemResultResponseMapper;
    
    /**
     * ItemManipulationResponseMapper constructor.
     *
     * @param ItemResultResponseMapper $itemResultResponseMapper
     */
    public function __construct(ItemResultResponseMapper $itemResultResponseMapper)
    {
        $this->itemResultResponseMapper = $itemResultResponseMapper;
    }
    
    /**
     * @inheritDoc
     */
    public function map(array $data): ItemManipulation
    {
        return new ItemManipulation(
            $data['status'],
            $this->itemResultResponseMapper->map($data['item']),
            $data['errors']
        );
    }
}
