<?php

namespace App\DNSDemo\ResponseMapper\Item;

use App\DNSDemo\DataObject\Item\ItemResult;
use App\DNSDemo\ResponseMapper\ResponseMapperInterface;
use App\DNSDemo\ResponseMapper\Zone\DomainDetailResponseMapper;

class ItemResultResponseMapper implements ResponseMapperInterface
{
    protected DomainDetailResponseMapper $domainDetailResponseMapper;
    
    /**
     * ItemResultResponseMapper constructor.
     *
     * @param DomainDetailResponseMapper $domainDetailResponseMapper
     */
    public function __construct(DomainDetailResponseMapper $domainDetailResponseMapper)
    {
        $this->domainDetailResponseMapper = $domainDetailResponseMapper;
    }
    
    /**
     * @inheritDoc
     */
    public function map(array $data): ItemResult
    {
        $item = new ItemResult();
        
        $item->setId($data['id']);
        $item->setType($data['type']);
        $item->setName($data['name']);
        $item->setContent($data['content']);
        $item->setTtl($data['ttl']);
        $item->setPriority($data['prio']);
        $item->setWeight($data['weight']);
        $item->setPort($data['port']);
        $item->setZone($this->domainDetailResponseMapper->map($data['zone']));
        
        return $item;
    }
}
