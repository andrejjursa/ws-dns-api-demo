<?php

namespace App\DNSDemo\ResponseMapper;

interface ResponseMapperInterface
{
    /**
     * @param array<mixed> $data
     * @return mixed
     */
    public function map(array $data);
}
