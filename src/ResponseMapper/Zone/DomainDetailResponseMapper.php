<?php

namespace App\DNSDemo\ResponseMapper\Zone;

use App\DNSDemo\DataObject\Zone\DomainDetail;
use App\DNSDemo\ResponseMapper\DateTimeResponseMapper;
use App\DNSDemo\ResponseMapper\ResponseMapperInterface;

class DomainDetailResponseMapper implements ResponseMapperInterface
{
    protected DateTimeResponseMapper $dateTimeResponseMapper;
    
    /**
     * DomainDetailResponseMapper constructor.
     *
     * @param DateTimeResponseMapper $dateTimeResponseMapper
     */
    public function __construct(DateTimeResponseMapper $dateTimeResponseMapper)
    {
        $this->dateTimeResponseMapper = $dateTimeResponseMapper;
    }
    
    /**
     * @inheritDoc
     */
    public function map(array $data): DomainDetail
    {
        return new DomainDetail(
            $data['id'] ?? $data['service_id'],
            $data['name'],
            $this->dateTimeResponseMapper->map(['time' => $data['updateTime']]),
        );
    }
}
