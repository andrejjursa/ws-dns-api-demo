<?php

namespace App\DNSDemo\ResponseMapper;

use App\DNSDemo\ResponseMapper\Item\ItemDetailResponseMapper;
use App\DNSDemo\ResponseMapper\Item\ItemListResponseMapper;
use App\DNSDemo\ResponseMapper\Item\ItemManipulationResponseMapper;
use App\DNSDemo\ResponseMapper\Item\ItemResultResponseMapper;
use App\DNSDemo\ResponseMapper\Item\SimpleItemResponseMapper;
use App\DNSDemo\ResponseMapper\Zone\DomainDetailResponseMapper;

class ResponseMapperFactory
{
    /**
     * @return DateTimeResponseMapper
     */
    public function getDateTimeResponseMapper(): DateTimeResponseMapper
    {
        return new DateTimeResponseMapper();
    }
    
    /**
     * @return PagerResponseMapper
     */
    public function getPagerResponseMapper(): PagerResponseMapper
    {
        return new PagerResponseMapper();
    }
    
    /**
     * @return DomainDetailResponseMapper
     */
    public function getZoneDomainDetailResponseMapper(): DomainDetailResponseMapper
    {
        return new DomainDetailResponseMapper($this->getDateTimeResponseMapper());
    }
    
    /**
     * @return SimpleItemResponseMapper
     */
    public function getItemSimpleItemResponseMapper(): SimpleItemResponseMapper
    {
        return new SimpleItemResponseMapper();
    }
    
    /**
     * @return ItemListResponseMapper
     */
    public function getItemItemListResponseMapper(): ItemListResponseMapper
    {
        return new ItemListResponseMapper(
            $this->getItemSimpleItemResponseMapper(),
            $this->getPagerResponseMapper()
        );
    }
    
    /**
     * @return ItemDetailResponseMapper
     */
    public function getItemItemDetailResponseMapper(): ItemDetailResponseMapper
    {
        return new ItemDetailResponseMapper(
            $this->getZoneDomainDetailResponseMapper(),
            $this->getItemSimpleItemResponseMapper()
        );
    }
    
    /**
     * @return ItemResultResponseMapper
     */
    public function getItemItemResultResponseMapper(): ItemResultResponseMapper
    {
        return new ItemResultResponseMapper($this->getZoneDomainDetailResponseMapper());
    }
    
    /**
     * @return ItemManipulationResponseMapper
     */
    public function getItemItemManipulationResponseMapper(): ItemManipulationResponseMapper
    {
        return new ItemManipulationResponseMapper($this->getItemItemResultResponseMapper());
    }
}
