<?php

namespace App\DNSDemo\ResponseMapper;

use App\DNSDemo\Exception\DateTimeMappingException;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;

class DateTimeResponseMapper implements ResponseMapperInterface
{
    /**
     * @inheritDoc
     */
    public function map(array $data): ?DateTimeInterface
    {
        try {
            if (isset($data['time'])) {
                return (new DateTimeImmutable())->setTimestamp($data['time']);
            }
            return null;
        } catch (Exception $e) {
            throw new DateTimeMappingException('Cannot map time: ' . $data['time']);
        }
    }
}
