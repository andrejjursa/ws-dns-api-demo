<?php

namespace App\DNSDemo\ResponseMapper;

use App\DNSDemo\DataObject\Pager;

class PagerResponseMapper implements ResponseMapperInterface
{
    /**
     * @inheritDoc
     */
    public function map(array $data): Pager
    {
        return new Pager(
            $data['page'],
            $data['pagesize'] ?? null,
            $data['items']
        );
    }
}
