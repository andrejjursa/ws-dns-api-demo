<?php

namespace App\DNSDemo\Service;

use App\DNSDemo\Connector\WSAPIConnector;
use App\DNSDemo\DataObject\Item\ItemDetail;
use App\DNSDemo\DataObject\Item\ItemList;
use App\DNSDemo\DataObject\Item\ItemManipulation;
use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Request\Item\ItemRequestFactory;
use App\DNSDemo\RequestMapper\RequestMapperFactory;
use App\DNSDemo\ResponseMapper\ResponseMapperFactory;
use App\DNSDemo\Traits\JSONStringToArrayTrait;
use JsonException;

class ItemService
{
    use JSONStringToArrayTrait;
    
    protected WSAPIConnector $WSAPIConnector;
    
    protected ItemRequestFactory $itemRequestFactory;
    
    protected ResponseMapperFactory $responseMapperFactory;
    
    protected RequestMapperFactory $requestMapperFactory;
    
    /**
     * ItemService constructor.
     *
     * @param WSAPIConnector $WSAPIConnector
     * @param ItemRequestFactory $itemRequestFactory
     * @param ResponseMapperFactory $responseMapperFactory
     * @param RequestMapperFactory $requestMapperFactory
     */
    public function __construct(
        WSAPIConnector $WSAPIConnector,
        ItemRequestFactory $itemRequestFactory,
        ResponseMapperFactory $responseMapperFactory,
        RequestMapperFactory $requestMapperFactory
    ) {
        $this->WSAPIConnector = $WSAPIConnector;
        $this->itemRequestFactory = $itemRequestFactory;
        $this->responseMapperFactory = $responseMapperFactory;
        $this->requestMapperFactory = $requestMapperFactory;
    }
    
    /**
     * @return ItemList
     * @throws ErrorResponseException
     * @throws JsonException
     */
    public function getItemList(): ItemList
    {
        $request = $this->itemRequestFactory->createGetItemListRequest();
        $response = $this->WSAPIConnector->call($request);
        
        if ($response->isResponseError()) {
            throw new ErrorResponseException($response->getResponseCode(), $response->getBody());
        }
        
        $responseArray = $this->jsonStringToArray($response->getBody());
        $mapper = $this->responseMapperFactory->getItemItemListResponseMapper();
        return $mapper->map($responseArray);
    }
    
    /**
     * @param int $id
     * @return ItemDetail
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function getItemDetail(int $id): ItemDetail
    {
        $request = $this->itemRequestFactory->createGetItemDetailRequest($id);
        $response = $this->WSAPIConnector->call($request);
        
        if ($response->isResponseError()) {
            throw new ErrorResponseException($response->getResponseCode(), $response->getBody());
        }
    
        $responseArray = $this->jsonStringToArray($response->getBody());
        $mapper = $this->responseMapperFactory->getItemItemDetailResponseMapper();
        return $mapper->map($responseArray);
    }
    
    /**
     * @param array<mixed> $data
     * @return ItemManipulation
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function createItem(array $data): ItemManipulation
    {
        $item = $this->requestMapperFactory->getItemItemRequestMapper()->map($data);
        $request = $this->itemRequestFactory->createPostItemRequest($item);
        $response = $this->WSAPIConnector->call($request);
        
        if ($response->isResponseError()) {
            throw new ErrorResponseException($response->getResponseCode(), $response->getBody());
        }
    
        $responseArray = $this->jsonStringToArray($response->getBody());
        $mapper = $this->responseMapperFactory->getItemItemManipulationResponseMapper();
        return $mapper->map($responseArray);
    }
    
    /**
     * @param int $id
     * @param array<mixed> $data
     * @return ItemManipulation
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function updateItem(int $id, array $data): ItemManipulation
    {
        $item = $this->requestMapperFactory->getItemItemRequestMapper()->map($data);
        $request = $this->itemRequestFactory->createPutItemRequest($id, $item);
        $response = $this->WSAPIConnector->call($request);
        
        if ($response->isResponseError()) {
            throw new ErrorResponseException($response->getResponseCode(), $response->getBody());
        }
    
        $responseArray = $this->jsonStringToArray($response->getBody());
        $mapper = $this->responseMapperFactory->getItemItemManipulationResponseMapper();
        return $mapper->map($responseArray);
    }
    
    /**
     * @param int $id
     * @return ItemManipulation
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function deleteItem(int $id): ItemManipulation
    {
        $request = $this->itemRequestFactory->createDeleteItemRequest($id);
        $response = $this->WSAPIConnector->call($request);
        
        if ($response->isResponseError()) {
            throw new ErrorResponseException($response->getResponseCode(), $response->getBody());
        }
    
        $responseArray = $this->jsonStringToArray($response->getBody());
        $mapper = $this->responseMapperFactory->getItemItemManipulationResponseMapper();
        return $mapper->map($responseArray);
    }
}
