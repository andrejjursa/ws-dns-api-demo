<?php

namespace App\DNSDemo\Service;

use App\DNSDemo\Connector\WSAPIConnector;
use App\DNSDemo\DataObject\Zone\DomainDetail;
use App\DNSDemo\Exception\ErrorResponseException;
use App\DNSDemo\Request\Zone\ZoneRequestFactory;
use App\DNSDemo\ResponseMapper\ResponseMapperFactory;
use App\DNSDemo\Traits\JSONStringToArrayTrait;
use JsonException;

class ZoneService
{
    use JSONStringToArrayTrait;
    
    protected WSAPIConnector $WSAPIConnector;
    
    protected ZoneRequestFactory $zoneRequestFactory;
    
    protected ResponseMapperFactory $responseMapperFactory;
    
    /**
     * ZoneService constructor.
     *
     * @param WSAPIConnector $WSAPIConnector
     * @param ZoneRequestFactory $zoneRequestFactory
     * @param ResponseMapperFactory $responseMapperFactory
     */
    public function __construct(
        WSAPIConnector $WSAPIConnector,
        ZoneRequestFactory $zoneRequestFactory,
        ResponseMapperFactory $responseMapperFactory
    ) {
        $this->WSAPIConnector = $WSAPIConnector;
        $this->zoneRequestFactory = $zoneRequestFactory;
        $this->responseMapperFactory = $responseMapperFactory;
    }
    
    /**
     * @throws JsonException
     * @throws ErrorResponseException
     */
    public function getZoneDetail(): DomainDetail
    {
        $request = $this->zoneRequestFactory->createGetZoneRequest();
        $response = $this->WSAPIConnector->call($request);
        
        if ($response->isResponseError()) {
            throw new ErrorResponseException($response->getResponseCode(), $response->getBody());
        }
        
        $responseArray = $this->jsonStringToArray($response->getBody());
        $mapper = $this->responseMapperFactory->getZoneDomainDetailResponseMapper();
        return $mapper->map($responseArray);
    }
}
