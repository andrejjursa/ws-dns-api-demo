<?php

namespace App\DNSDemo\Request\Item;

use App\DNSDemo\Connector\Request\AbstractPOSTAPIRequest;
use App\DNSDemo\DataObject\Item\Item;

class PostItemRequest extends AbstractPOSTAPIRequest
{
    protected string $userId;
    
    protected string $domain;
    
    protected Item $item;
    
    /**
     * PostItemRequest constructor.
     *
     * @param string $userId
     * @param string $domain
     * @param Item $item
     */
    public function __construct(string $userId, string $domain, Item $item)
    {
        $this->userId = $userId;
        $this->domain = $domain;
        $this->item = $item;
    }
    
    /**
     * @inheritDoc
     */
    public function getPath(): string
    {
        return sprintf(
            '/v1/user/%s/zone/%s/record',
            $this->userId,
            $this->domain
        );
    }
    
    /**
     * @inheritDoc
     */
    public function getData(): ?array
    {
        return $this->item->toArray();
    }
}
