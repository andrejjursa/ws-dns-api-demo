<?php

namespace App\DNSDemo\Request\Item;

use App\DNSDemo\Connector\Request\AbstractGETAPIRequest;

class GetItemDetailRequest extends AbstractGETAPIRequest
{
    protected string $userId;
    
    protected string $domain;
    
    protected int $id;
    
    /**
     * GetItemDetailRequest constructor.
     *
     * @param string $userId
     * @param string $domain
     * @param int $id
     */
    public function __construct(string $userId, string $domain, int $id)
    {
        $this->userId = $userId;
        $this->domain = $domain;
        $this->id = $id;
    }
    
    /**
     * @inheritDoc
     */
    public function getPath(): string
    {
        return sprintf(
            '/v1/user/%s/zone/%s/record/%d',
            $this->userId,
            $this->domain,
            $this->id
        );
    }
}
