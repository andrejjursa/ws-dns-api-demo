<?php

namespace App\DNSDemo\Request\Item;

use App\DNSDemo\Connector\Request\AbstractPUTAPIRequest;
use App\DNSDemo\DataObject\Item\Item;

class PutItemRequest extends AbstractPUTAPIRequest
{
    protected string $userId;
    
    protected string $domain;
    
    protected int $id;
    
    protected Item $data;
    
    /**
     * PutItemRequest constructor.
     *
     * @param string $userId
     * @param string $domain
     * @param int $id
     * @param Item $data
     */
    public function __construct(string $userId, string $domain, int $id, Item $data)
    {
        $this->userId = $userId;
        $this->domain = $domain;
        $this->id = $id;
        $this->data = $data;
    }
    
    /**
     * @inheritDoc
     */
    public function getPath(): string
    {
        return sprintf(
            '/v1/user/%s/zone/%s/record/%d',
            $this->userId,
            $this->domain,
            $this->id
        );
    }
    
    /**
     * @inheritDoc
     */
    public function getData(): ?array
    {
        $dataArray = $this->data->toArray();
        if (array_key_exists('type', $dataArray)) {
            unset($dataArray['type']); // this parameter is not allowed for record update
        }
        return $dataArray;
    }
}
