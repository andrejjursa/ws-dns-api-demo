<?php

namespace App\DNSDemo\Request\Item;

use App\DNSDemo\Connector\Request\AbstractGETAPIRequest;

class GetItemListRequest extends AbstractGETAPIRequest
{
    protected string $userId;
    
    protected string $domain;
    
    /**
     * GetItemListRequest constructor.
     *
     * @param string $userId
     * @param string $domain
     */
    public function __construct(string $userId, string $domain)
    {
        $this->userId = $userId;
        $this->domain = $domain;
    }
    
    /**
     * @inheritDoc
     */
    public function getPath(): string
    {
        return sprintf(
            '/v1/user/%s/zone/%s/record',
            $this->userId,
            $this->domain
        );
    }
}
