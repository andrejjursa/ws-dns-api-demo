<?php

namespace App\DNSDemo\Request\Item;

use App\DNSDemo\DataObject\Item\Item;

class ItemRequestFactory
{
    protected string $userId;
    
    protected string $domain;
    
    /**
     * ItemRequestFactory constructor.
     *
     * @param string $userId
     * @param string $domain
     */
    public function __construct(string $userId, string $domain)
    {
        $this->userId = $userId;
        $this->domain = $domain;
    }
    
    /**
     * @return GetItemListRequest
     */
    public function createGetItemListRequest(): GetItemListRequest
    {
        return new GetItemListRequest(
            $this->userId,
            $this->domain
        );
    }
    
    /**
     * @param int $id
     * @return GetItemDetailRequest
     */
    public function createGetItemDetailRequest(int $id): GetItemDetailRequest
    {
        return new GetItemDetailRequest(
            $this->userId,
            $this->domain,
            $id
        );
    }
    
    /**
     * @param Item $item
     * @return PostItemRequest
     */
    public function createPostItemRequest(Item $item): PostItemRequest
    {
        return new PostItemRequest(
            $this->userId,
            $this->domain,
            $item
        );
    }
    
    /**
     * @param int $id
     * @param Item $item
     * @return PutItemRequest
     */
    public function createPutItemRequest(int $id, Item $item): PutItemRequest
    {
        return new PutItemRequest(
            $this->userId,
            $this->domain,
            $id,
            $item
        );
    }
    
    /**
     * @param int $id
     * @return DeleteItemRequest
     */
    public function createDeleteItemRequest(int $id): DeleteItemRequest
    {
        return new DeleteItemRequest(
            $this->userId,
            $this->domain,
            $id
        );
    }
}
