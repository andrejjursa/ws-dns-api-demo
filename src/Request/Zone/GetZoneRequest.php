<?php

namespace App\DNSDemo\Request\Zone;

use App\DNSDemo\Connector\Request\AbstractGETAPIRequest;

class GetZoneRequest extends AbstractGETAPIRequest
{
    protected string $userId;
    
    protected string $domain;
    
    /**
     * GetZoneRequest constructor.
     *
     * @param string $userId
     * @param string $domain
     */
    public function __construct(string $userId, string $domain)
    {
        $this->userId = $userId;
        $this->domain = $domain;
    }
    
    /**
     * @inheritDoc
     */
    public function getPath(): string
    {
        return sprintf(
            '/v1/user/%s/zone/%s',
            $this->userId,
            $this->domain
        );
    }
}
