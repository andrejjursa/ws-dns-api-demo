<?php

namespace App\DNSDemo\Request\Zone;

class ZoneRequestFactory
{
    protected string $userId;
    
    protected string $domain;
    
    /**
     * ZoneRequestFactory constructor.
     *
     * @param string $userId
     * @param string $domain
     */
    public function __construct(string $userId, string $domain)
    {
        $this->userId = $userId;
        $this->domain = $domain;
    }
    
    public function createGetZoneRequest(): GetZoneRequest
    {
        return new GetZoneRequest(
            $this->userId,
            $this->domain
        );
    }
}
