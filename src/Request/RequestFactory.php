<?php

namespace App\DNSDemo\Request;

use App\DNSDemo\Request\Item\ItemRequestFactory;
use App\DNSDemo\Request\Zone\ZoneRequestFactory;

class RequestFactory
{
    protected ZoneRequestFactory $zoneRequestFactory;
    
    protected ItemRequestFactory $itemRequestFactory;
    
    /**
     * RequestFactory constructor.
     *
     * @param ZoneRequestFactory $zoneRequestFactory
     * @param ItemRequestFactory $itemRequestFactory
     */
    public function __construct(ZoneRequestFactory $zoneRequestFactory, ItemRequestFactory $itemRequestFactory)
    {
        $this->zoneRequestFactory = $zoneRequestFactory;
        $this->itemRequestFactory = $itemRequestFactory;
    }
    
    /**
     * @return ZoneRequestFactory
     */
    public function getZone(): ZoneRequestFactory
    {
        return $this->zoneRequestFactory;
    }
    
    /**
     * @return ItemRequestFactory
     */
    public function getItem(): ItemRequestFactory
    {
        return $this->itemRequestFactory;
    }
}
