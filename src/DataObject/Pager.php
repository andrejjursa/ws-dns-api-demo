<?php

namespace App\DNSDemo\DataObject;

class Pager implements DataObjectInterface
{
    protected int $page;
    
    protected ?int $pageSize;
    
    protected int $items;
    
    /**
     * Pager constructor.
     *
     * @param int $page
     * @param int|null $pageSize
     * @param int $items
     */
    public function __construct(int $page, ?int $pageSize, int $items)
    {
        $this->page = $page;
        $this->pageSize = $pageSize;
        $this->items = $items;
    }
    
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
    
    /**
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }
    
    /**
     * @return int
     */
    public function getItems(): int
    {
        return $this->items;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'page' => $this->getPage(),
            'pagesize' => $this->getPageSize(),
            'items' => $this->getItems(),
        ];
    }
}
