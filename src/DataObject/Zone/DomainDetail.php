<?php

namespace App\DNSDemo\DataObject\Zone;

use App\DNSDemo\DataObject\DataObjectInterface;
use DateTimeInterface;

class DomainDetail implements DataObjectInterface
{
    protected int $id;
    
    protected string $name;
    
    protected DateTimeInterface $updateTime;
    
    /**
     * DomainDetail constructor.
     *
     * @param int $id
     * @param string $name
     * @param DateTimeInterface $updateTime
     */
    public function __construct(int $id, string $name, DateTimeInterface $updateTime)
    {
        $this->id = $id;
        $this->name = $name;
        $this->updateTime = $updateTime;
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @return DateTimeInterface
     */
    public function getUpdateTime(): DateTimeInterface
    {
        return $this->updateTime;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'updateTime' => (int)$this->getUpdateTime()->format('U'),
        ];
    }
}
