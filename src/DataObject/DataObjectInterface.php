<?php

namespace App\DNSDemo\DataObject;

interface DataObjectInterface
{
    /**
     * @return array<mixed>
     */
    public function toArray(): array;
}
