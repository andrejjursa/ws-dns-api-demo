<?php

namespace App\DNSDemo\DataObject\Item;

use App\DNSDemo\DataObject\DataObjectInterface;

class ItemManipulation implements DataObjectInterface
{
    protected string $status;
    
    protected ItemResult $item;
    
    /**
     * @var array<string, array<string>>
     */
    protected array $errors = [];
    
    /**
     * ItemManipulationResponse constructor.
     *
     * @param string $status
     * @param ItemResult $item
     * @param array<string, array<string>> $errors
     */
    public function __construct(string $status, ItemResult $item, array $errors)
    {
        $this->status = $status;
        $this->item = $item;
        $this->errors = $errors;
    }
    
    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
    
    /**
     * @return ItemResult
     */
    public function getItem(): ItemResult
    {
        return $this->item;
    }
    
    /**
     * @return array<string, array<string>>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'status' => $this->getStatus(),
            'item' => $this->getItem()->toArray(),
            'errors' => $this->getErrors(),
        ];
    }
}
