<?php

namespace App\DNSDemo\DataObject\Item;

use App\DNSDemo\DataObject\DataObjectInterface;

class SimpleItem implements DataObjectInterface
{
    protected ?int $id;
    
    protected string $type;
    
    protected string $name;
    
    protected string $content;
    
    protected int $ttl;
    
    protected ?int $priority = null;
    
    protected ?int $weight = null;
    
    protected ?int $port = null;
    
    /**
     * SimpleItem constructor.
     *
     * @param int|null $id
     * @param string $type
     * @param string $name
     * @param string $content
     * @param int $ttl
     */
    public function __construct(?int $id, string $type, string $name, string $content, int $ttl)
    {
        $this->id = $id;
        $this->type = $type;
        $this->name = $name;
        $this->content = $content;
        $this->ttl = $ttl;
    }
    
    /**
     * @param int|null $priority
     * @return SimpleItem
     */
    public function setPriority(?int $priority): SimpleItem
    {
        $this->priority = $priority;
        return $this;
    }
    
    /**
     * @param int|null $weight
     * @return SimpleItem
     */
    public function setWeight(?int $weight): SimpleItem
    {
        $this->weight = $weight;
        return $this;
    }
    
    /**
     * @param int|null $port
     * @return SimpleItem
     */
    public function setPort(?int $port): SimpleItem
    {
        $this->port = $port;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
    
    /**
     * @return int
     */
    public function getTtl(): int
    {
        return $this->ttl;
    }
    
    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }
    
    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'name' => $this->getName(),
            'content' => $this->getContent(),
            'ttl' => $this->getTtl(),
            'prio' => $this->getPriority(),
            'weight' => $this->getWeight(),
            'port' => $this->getPort(),
        ];
    }
}
