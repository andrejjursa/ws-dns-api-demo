<?php

namespace App\DNSDemo\DataObject\Item;

use App\DNSDemo\DataObject\Zone\DomainDetail;

class ItemDetail extends SimpleItem
{
    protected DomainDetail $zone;
    
    /**
     * @param DomainDetail $zone
     * @return ItemDetail
     */
    public function setZone(DomainDetail $zone): ItemDetail
    {
        $this->zone = $zone;
        return $this;
    }
    
    /**
     * @return DomainDetail
     */
    public function getZone(): DomainDetail
    {
        return $this->zone;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $itemData = parent::toArray();
        $itemData['zone'] = $this->getZone()->toArray();
        
        return $itemData;
    }
}
