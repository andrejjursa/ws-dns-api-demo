<?php

namespace App\DNSDemo\DataObject\Item;

use App\DNSDemo\DataObject\DataObjectInterface;
use App\DNSDemo\DataObject\Pager;

class ItemList implements DataObjectInterface
{
    /**
     * @var array<SimpleItem>
     */
    protected array $items = [];
    
    protected Pager $pager;
    
    /**
     * ItemList constructor.
     *
     * @param array<SimpleItem> $items
     * @param Pager $pager
     */
    public function __construct(array $items, Pager $pager)
    {
        $this->items = $items;
        $this->pager = $pager;
    }
    
    /**
     * @return array<SimpleItem>
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @return Pager
     */
    public function getPager(): Pager
    {
        return $this->pager;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'items' => array_map(static function (SimpleItem $item) { return $item->toArray(); }, $this->getItems()),
            'pager' => $this->getPager()->toArray(),
        ];
    }
}
