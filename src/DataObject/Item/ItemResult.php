<?php

namespace App\DNSDemo\DataObject\Item;

use App\DNSDemo\DataObject\Zone\DomainDetail;

class ItemResult extends Item
{
    protected ?int $id = null;
    
    protected DomainDetail $zone;
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @param int|null $id
     * @return ItemResult
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return DomainDetail
     */
    public function getZone(): DomainDetail
    {
        return $this->zone;
    }
    
    /**
     * @param DomainDetail $zone
     * @return ItemResult
     */
    public function setZone(DomainDetail $zone): self
    {
        $this->zone = $zone;
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'name' => $this->getName(),
            'content' => $this->getContent(),
            'ttl' => $this->getTtl(),
            'prio' => $this->getPriority(),
            'weight' => $this->getWeight(),
            'port' => $this->getPort(),
            'zone' => $this->getZone()->toArray(),
        ];
    }
}
