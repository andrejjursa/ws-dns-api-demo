<?php

namespace App\DNSDemo\DataObject\Item;

use App\DNSDemo\DataObject\DataObjectInterface;

class Item implements DataObjectInterface
{
    protected ?string $type = null;
    
    protected ?string $name = null;
    
    protected ?string $content = null;
    
    protected ?int $ttl = null;
    
    protected ?int $priority = null;
    
    protected ?int $weight = null;
    
    protected ?int $port = null;
    
    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    
    /**
     * @param string|null $type
     * @return Item
     */
    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return Item
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }
    
    /**
     * @param string|null $content
     * @return Item
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getTtl(): ?int
    {
        return $this->ttl;
    }
    
    /**
     * @param int|null $ttl
     * @return Item
     */
    public function setTtl(?int $ttl): self
    {
        $this->ttl = $ttl;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    /**
     * @param int|null $priority
     * @return Item
     */
    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }
    
    /**
     * @param int|null $weight
     * @return Item
     */
    public function setWeight(?int $weight): self
    {
        $this->weight = $weight;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }
    
    /**
     * @param int|null $port
     * @return Item
     */
    public function setPort(?int $port): Item
    {
        $this->port = $port;
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $array = [];
        
        if (null !== $this->getType()) {
            $array['type'] = $this->getType();
        }
        
        if (null !== $this->getName()) {
            $array['name'] = $this->getName();
        }
        
        if (null !== $this->getContent()) {
            $array['content'] = $this->getContent();
        }
        
        if (null !== $this->getTtl()) {
            $array['ttl'] = $this->getTtl();
        }
        
        if (null !== $this->getWeight()) {
            $array['weight'] = $this->getWeight();
        }
        
        if (null !== $this->getPriority()) {
            $array['prio'] = $this->getPriority();
        }
        
        if (null !== $this->getPort()) {
            $array['port'] = $this->getPort();
        }
        
        return $array;
    }
}
