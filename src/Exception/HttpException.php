<?php

namespace App\DNSDemo\Exception;

use App\DNSDemo\Application\HTTP\Response;
use Exception;
use Throwable;

class HttpException extends Exception
{
    protected int $statusCode = Response::HTTP_OK;
    
    /**
     * HttpException constructor.
     *
     * @param int $statusCode
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(int $statusCode, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->statusCode = $statusCode;
        parent::__construct($message, $code, $previous);
    }
    
    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
    
    /**
     * @param int $statusCode
     * @return HttpException
     */
    public function setStatusCode(int $statusCode): HttpException
    {
        $this->statusCode = $statusCode;
        return $this;
    }
}
