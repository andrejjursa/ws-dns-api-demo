<?php

namespace App\DNSDemo\Exception;

use Exception;

class ContainerContentNotSetException extends Exception
{
}
