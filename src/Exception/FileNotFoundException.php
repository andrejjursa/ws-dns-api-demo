<?php

namespace App\DNSDemo\Exception;

use RuntimeException;

class FileNotFoundException extends RuntimeException
{
}
