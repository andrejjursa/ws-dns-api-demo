<?php

namespace App\DNSDemo\ComponentFactory\Connector;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Connector\WSAPIConnector;

class WSAPIConnectorFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): WSAPIConnector
    {
        $config = $container->get('config');
        
        return new WSAPIConnector(
            $config['apiURL'],
            $config['apiKey'],
            $config['secret']
        );
    }
}
