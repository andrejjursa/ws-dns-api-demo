<?php

namespace App\DNSDemo\ComponentFactory\Service;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Connector\WSAPIConnector;
use App\DNSDemo\Request\Item\ItemRequestFactory;
use App\DNSDemo\RequestMapper\RequestMapperFactory;
use App\DNSDemo\ResponseMapper\ResponseMapperFactory;
use App\DNSDemo\Service\ItemService;

class ItemServiceFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): ItemService
    {
        return new ItemService(
            $container->get(WSAPIConnector::class),
            $container->get(ItemRequestFactory::class),
            $container->get(ResponseMapperFactory::class),
            $container->get(RequestMapperFactory::class)
        );
    }
}
