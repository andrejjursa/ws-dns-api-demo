<?php

namespace App\DNSDemo\ComponentFactory\Service;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Connector\WSAPIConnector;
use App\DNSDemo\Request\Zone\ZoneRequestFactory;
use App\DNSDemo\ResponseMapper\ResponseMapperFactory;
use App\DNSDemo\Service\ZoneService;

class ZoneServiceFactory implements FactoryInterface
{
    
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): ZoneService
    {
        return new ZoneService(
            $container->get(WSAPIConnector::class),
            $container->get(ZoneRequestFactory::class),
            $container->get(ResponseMapperFactory::class)
        );
    }
}
