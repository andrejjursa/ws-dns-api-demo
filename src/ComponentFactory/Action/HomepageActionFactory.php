<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\HomepageAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;

class HomepageActionFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): HomepageAction
    {
        $config = $container->get('config');
        return new HomepageAction($config['domain']);
    }
}
