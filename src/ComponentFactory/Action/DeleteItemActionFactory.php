<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\DeleteItemAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Service\ItemService;

class DeleteItemActionFactory implements FactoryInterface
{
    
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): DeleteItemAction
    {
        return new DeleteItemAction($container->get(ItemService::class));
    }
}
