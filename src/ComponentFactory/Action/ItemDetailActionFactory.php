<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\ItemDetailAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Service\ItemService;

class ItemDetailActionFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): ItemDetailAction
    {
        return new ItemDetailAction($container->get(ItemService::class));
    }
}
