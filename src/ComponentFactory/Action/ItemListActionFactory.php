<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\ItemListAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Service\ItemService;

class ItemListActionFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): ItemListAction
    {
        return new ItemListAction($container->get(ItemService::class));
    }
}
