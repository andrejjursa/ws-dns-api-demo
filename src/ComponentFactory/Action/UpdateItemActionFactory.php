<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\UpdateItemAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Service\ItemService;

class UpdateItemActionFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): UpdateItemAction
    {
        return new UpdateItemAction($container->get(ItemService::class));
    }
}
