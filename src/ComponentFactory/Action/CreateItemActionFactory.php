<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\CreateItemAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Service\ItemService;

class CreateItemActionFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): CreateItemAction
    {
        return new CreateItemAction($container->get(ItemService::class));
    }
}
