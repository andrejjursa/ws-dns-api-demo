<?php

namespace App\DNSDemo\ComponentFactory\Action;

use App\DNSDemo\Action\DomainDetailsAction;
use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Service\ZoneService;

class DomainDetailsActionFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): DomainDetailsAction
    {
        return new DomainDetailsAction(
            $container->get(ZoneService::class)
        );
    }
}
