<?php

namespace App\DNSDemo\ComponentFactory\Request\Item;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Request\Item\ItemRequestFactory;

class ItemRequestFactoryFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): ItemRequestFactory
    {
        $config = $container->get('config');
        return new ItemRequestFactory(
            $config['userId'],
            $config['domain']
        );
    }
}
