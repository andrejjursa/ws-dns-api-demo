<?php

namespace App\DNSDemo\ComponentFactory\Request;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Request\Item\ItemRequestFactory;
use App\DNSDemo\Request\RequestFactory;
use App\DNSDemo\Request\Zone\ZoneRequestFactory;

class RequestFactoryFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): RequestFactory
    {
        return new RequestFactory(
            $container->get(ZoneRequestFactory::class),
            $container->get(ItemRequestFactory::class)
        );
    }
}
