<?php

namespace App\DNSDemo\ComponentFactory\Request\Zone;

use App\DNSDemo\Application\Container\ContainerInterface;
use App\DNSDemo\Application\Container\FactoryInterface;
use App\DNSDemo\Request\Zone\ZoneRequestFactory;

class ZoneRequestFactoryFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container): ZoneRequestFactory
    {
        $config = $container->get('config');
        return new ZoneRequestFactory(
            $config['userId'],
            $config['domain']
        );
    }
}
