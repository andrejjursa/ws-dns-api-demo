<html>
<head>
    <title>DNS settings of <?= $param['domain'] ?? 'unknown domain' ?>!</title>
    <meta charset="UTF-8">
    <script type="text/javascript" src="js/application.js"></script>
    <link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
    <h1>DNS settings!</h1>
    
    <div id="domainDetails"></div>

    <div id="newRecordSelector">
        <h2>Create new DNS record</h2>
        <form id="newRecordSelector-form">
            <label>
                <div class="form-label">Select record type:</div>
                <div class="form-element">
                    <select name="type">
                        <option value="">...</option>
                        <option value="A">A</option>
                        <option value="AAAA">AAAA</option>
                        <option value="MX">MX</option>
                        <option value="ANAME">ANAME</option>
                        <option value="CNAME">CNAME</option>
                        <option value="NS">NS</option>
                        <option value="TXT">TXT</option>
                        <option value="SRV">SRV</option>
                    </select>
                </div>
            </label>
        </form>
        <div id="newRecord"></div>
    </div>

    <div id="recordsWrapping">
        <h2>Existing records</h2>
        <div id="records"></div>
    </div>
    
    <script type="text/javascript">
        var application = new Application();
        application.run();
    </script>
</body>
</html>
