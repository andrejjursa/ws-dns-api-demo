function Application() {
    return this;
}

Application.prototype.run = function () {
    this.newRecordListener();
    this.domainDetails();
    this.recordsList();
};

Application.prototype.makeAPIRequest = function (endpoint, method, data, onload, onerror) {
    var url = '/api/v1' + endpoint;
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = function () {
        onload(xhr.response, xhr.status);
    };
    xhr.onerror = onerror;
    xhr.responseType = "json";
    xhr.send(data);
};

Application.prototype.domainDetails = function () {
    var detailParent = document.getElementById('domainDetails');
    var success = function (response, status) {
        detailParent.innerHTML = '';

        if (status === 200) {
            var domainBox = document.createElement('div');
            domainBox.setAttribute('class', 'domainDetails-name');

            var domainBoxLabel = document.createElement('span');
            domainBoxLabel.setAttribute('class', 'domainDetails-name-label');

            var domainBoxLabelText = document.createTextNode('Domain name:');
            domainBoxLabel.appendChild(domainBoxLabelText);

            var domainBoxContent = document.createElement('span');
            domainBoxContent.setAttribute('class', 'domainDetails-name-value');

            var domainName = document.createTextNode(response.name);
            domainBoxContent.appendChild(domainName);

            domainBox.appendChild(domainBoxLabel);
            domainBox.appendChild(domainBoxContent);

            detailParent.appendChild(domainBox);
            return;
        }

        var errorBox = document.createElement('div');
        errorBox.setAttribute('class', 'error');

        var errorMessage = document.createTextNode('Domain data unobtainable.');
        errorBox.appendChild(errorMessage);

        detailParent.appendChild(errorBox);
    };
    var error = function () {
        detailParent.innerHTML = '';

        var errorBox = document.createElement('div');
        errorBox.setAttribute('class', 'error');

        var errorMessage = document.createTextNode('Domain data can not be downloaded now.');
        errorBox.appendChild(errorMessage);

        detailParent.appendChild(errorBox);
    };
    this.makeAPIRequest('/zone', 'GET', '', success, error);
};

Application.prototype.recordsList = function () {
    var self = this;
    var records = document.getElementById('records');
    var success = function (response, status) {
        if (status === 200) {
            self.renderRecords(records, response.items);
        }
    };
    var error = function () {
        records.innerHTML = '';

        var errorBox = document.createElement('div');
        errorBox.setAttribute('class', 'error');

        var errorMessage = document.createTextNode('Records can not be downloaded now.');
        errorBox.appendChild(errorMessage);

        records.appendChild(errorBox);
    }
    this.makeAPIRequest('/item', 'GET', '', success, error);
};

Application.prototype.renderRecords = function (recordsElement, recordsList) {
    recordsElement.innerHTML = '';

    for (var i in recordsList) {
        var item = recordsList[i];

        recordsElement.appendChild(this.renderRecord(item.type, item, {}, true));
    }
};

Application.prototype.renderRecord = function (recordType, data, errors, addDelete) {
    var recordID = null;
    var container = document.createElement('div');
    container.setAttribute('class', 'record');
    if (typeof data.id !== 'undefined' && data.id !== null) {
        container.setAttribute('id', 'record-id-' + data.id);
        container.setAttribute('data-id', data.id);
        recordID = data.id;
    }
    container.setAttribute('data-type', recordType);

    var innerForm = document.createElement('form');
    innerForm.setAttribute('name', 'record-form');
    container.appendChild(innerForm);

    innerForm.appendChild(this.renderRecordType(recordType));

    innerForm.appendChild(this.renderRecordField(
        'name',
        typeof data.name !== 'undefined' ? data.name : '',
        true,
        errors
        )
    );
    innerForm.appendChild(this.renderRecordField(
        'content',
        typeof data.content !== 'undefined' ? data.content : '',
        true,
        errors
        )
    );
    if (recordType === 'MX' || recordType === 'SRV') {
        innerForm.appendChild(this.renderRecordField(
            'prio',
            typeof data.prio !== 'undefined' ? data.prio : '',
            true,
            errors
        ));

    }
    if (recordType === 'SRV') {
        innerForm.appendChild(this.renderRecordField(
            'port',
            typeof data.port !== 'undefined' ? data.port : '',
            true,
            errors
        ));
        innerForm.appendChild(this.renderRecordField(
            'weight',
            typeof data.weight !== 'undefined' ? data.weight : '',
            true,
            errors
        ));
    }
    innerForm.appendChild(this.renderRecordField(
        'ttl',
        typeof data.ttl !== 'undefined' ? data.ttl : '',
        false,
        errors
        )
    );

    innerForm.appendChild(this.renderRecordButtons(recordID, innerForm, addDelete));

    return container;
};

Application.prototype.renderRecordField = function (fieldType, fieldValue, required, errors) {
    var container = document.createElement('div');
    container.setAttribute('class', 'record-' + fieldType);
    container.setAttribute('data-type', fieldType);

    var label = document.createElement('label');
    container.appendChild(label);

    var fieldDiv = document.createElement('div');
    fieldDiv.setAttribute('class', 'record-label record-label-' + fieldType);

    var labelText = '';
    switch (fieldType) {
        case 'name': labelText = 'Name:'; break;
        case 'content': labelText = 'Content:'; break;
        case 'ttl': labelText = 'TTL:'; break;
        case 'prio': labelText = 'Priority:'; break;
        case 'weight': labelText = 'Weight:'; break;
        case 'port': labelText = 'Port:'; break;
    }
    fieldDiv.innerText = required === true ? '* ' + labelText : labelText;
    label.appendChild(fieldDiv);

    var valueWrapClass = 'record-value-wrap record-value-wrap-' + fieldType;
    if (this.isError(errors, fieldType)) {
        valueWrapClass += ' record-value-error';
    }
    var valueWrap = document.createElement('div');
    valueWrap.setAttribute('class', valueWrapClass);
    label.appendChild(valueWrap);

    var value = document.createElement('input');
    if (fieldType === 'ttl' || fieldType === 'prio' || fieldType === 'weight' || fieldType === 'port') {
        value.setAttribute('type', 'number');
    } else {
        value.setAttribute('type', 'text');
    }
    if (required) {
        value.setAttribute('required', 'required');
    }
    value.setAttribute('data-value-type', fieldType);

    if (fieldValue !== undefined && fieldValue !== null) {
        value.setAttribute('value', fieldValue);
    }

    valueWrap.appendChild(value);

    if (this.isError(errors, fieldType)) {
        container.appendChild(this.renderErrors(errors, fieldType));
    }

    return container;
};

Application.prototype.isError = function (errors, fieldType) {
    return typeof errors[fieldType] !== "undefined";
};

Application.prototype.renderErrors = function (errors, fieldType) {
    var container = document.createElement('ul');
    container.setAttribute('class', 'record-value-errors');

    var fieldErrors = typeof errors[fieldType] !== 'undefined' ? errors[fieldType] : [];

    for (var i in fieldErrors) {
        var fieldError = fieldErrors[i];

        var errorItem = document.createElement('li');
        errorItem.innerText = fieldError;
        container.appendChild(errorItem);
    }

    return container;
};

Application.prototype.renderRecordType = function (recordType) {
    var container = document.createElement('div');
    container.setAttribute('class', 'record-type');

    var label = document.createElement('label');
    container.appendChild(label);

    var typeDivLabel = document.createElement('div');
    typeDivLabel.setAttribute('class', 'record-label record-label-type');
    typeDivLabel.innerText = 'Type:';
    label.appendChild(typeDivLabel);

    var typeDivValue = document.createElement('div');
    typeDivValue.setAttribute('class', 'record-value record-value-type');
    typeDivValue.innerText = recordType;
    label.appendChild(typeDivValue);

    return container;
};

Application.prototype.renderRecordButtons = function (id, innerForm, addDelete) {
    var self = this;
    var container = document.createElement('div');
    container.setAttribute('class', 'record-buttons');

    var saveButton = document.createElement('button');
    saveButton.setAttribute('class', 'record-button-save');
    saveButton.innerText = 'Save';
    if (addDelete === true) {
        saveButton.addEventListener('click', function (event) {
            event.preventDefault();
            var data = self.obtainData(innerForm.parentNode);
            var id = innerForm.parentNode.getAttribute('data-id');
            self.setRecordFieldDisabled(innerForm);
            self.updateRecord(id, data);
        });
    } else {
        saveButton.addEventListener('click', function (event) {
            event.preventDefault();
            var data = self.obtainData(innerForm.parentNode);
            self.setRecordFieldDisabled(innerForm);
            self.setNewRecordSelectDisabled(true);
            self.createRecord(data);
        });
    }
    container.appendChild(saveButton);

    if (addDelete === true) {
        var deleteButton = document.createElement('button');
        deleteButton.setAttribute('class', 'record-button-delete');
        deleteButton.innerText = 'Delete';
        deleteButton.addEventListener('click', function (event) {
            event.preventDefault();
            var id = innerForm.parentNode.getAttribute('data-id');
            var data = self.obtainData(innerForm.parentNode);
            var answer = confirm('Are you sure you want to delete record of type ' + data.type + '?');
            if (answer === true) {
                self.setRecordFieldDisabled(innerForm);
                self.deleteRecord(id);
            }
        });
        container.appendChild(deleteButton);
    }

    return container;
};

Application.prototype.createRecord = function (data) {
    var self = this;
    var success = function (response, status) {
        self.setNewRecordSelectDisabled(false);
        var newRecordDiv = document.getElementById('newRecord');
        if (status === 200 || status === 201) {
            if (response.status === 'error') {
                newRecordDiv.innerHTML = '';
                newRecordDiv.appendChild(self.renderRecord(response.item.type, response.item, response.errors, false));
                return;
            }
            newRecordDiv.innerHTML = '';
            var records = document.getElementById('records');
            records.appendChild(self.renderRecord(response.item.type, response.item, [], true));
            alert('New record of type ' + response.item.type + ' was created!');
            return;
        }
        self.setRecordFieldDisabled(newRecordDiv, false);
        if (typeof response.error === 'string') {
            alert(response.error);
        }
        else if (typeof response.error === 'object' && typeof response.error.message === 'string') {
            alert(response.error.message);
        }
    };
    var error = function () {
        self.setNewRecordSelectDisabled(false);
        var newRecordDiv = document.getElementById('newRecord');
        self.setRecordFieldDisabled(newRecordDiv, false);
        alert('Record create operation failed. Try again later.');
    };
    this.makeAPIRequest('/item', 'POST', JSON.stringify(data), success, error);
}

Application.prototype.updateRecord = function (id, data) {
    var self = this;
    var success = function (response, status) {
        var originalRecord = document.getElementById('record-id-' + id);
        if (status === 200) {
            var newRecord = self.renderRecord(response.item.type, response.item, response.errors, true);

            originalRecord.parentNode.replaceChild(newRecord, originalRecord);
            alert('Record updated!');
            return;
        }
        self.setRecordFieldDisabled(originalRecord, false);
        if (typeof response.error === 'string') {
            alert(response.error);
        }
        else if (typeof response.error === 'object' && typeof response.error.message === 'string') {
            alert(response.error.message);
        }
    };
    var error = function () {
        var originalRecord = document.getElementById('record-id-' + id);
        self.setRecordFieldDisabled(originalRecord, false);
        alert('Record update operation failed. Try again later!');
    };
    this.makeAPIRequest('/item/' + id, 'PUT', JSON.stringify(data), success, error);
};

Application.prototype.deleteRecord = function (id)
{
    var self = this;
    var success = function (response, status) {
        var originalRecord = document.getElementById('record-id-' + id);
        if (status === 200) {
            originalRecord.parentNode.removeChild(originalRecord);
            alert('Record deleted!');
            return;
        }
        self.setRecordFieldDisabled(originalRecord, false);
        if (typeof response.error === 'string') {
            alert(response.error);
        }
        else if (typeof response.error === 'object' && typeof response.error.message === 'string') {
            alert(response.error.message);
        }
    };
    var error = function () {
        var originalRecord = document.getElementById('record-id-' + id);
        self.setRecordFieldDisabled(originalRecord, false);
        alert('Record delete operation failed. Try again later!');
    };
    this.makeAPIRequest('/item/' + id, 'DELETE', '', success, error);
};

Application.prototype.obtainData = function (recordWrap) {
    var type = recordWrap.getAttribute('data-type');
    var data = {
        'type': type
    };

    var inputs = recordWrap.getElementsByTagName('input');

    for (var i in inputs) {
        var input = inputs[i];
        var inputType = '';
        for (var e in input.attributes) {
            var attribute = input.attributes[e];
            if (attribute.name === 'data-value-type') {
                inputType = attribute.value;
                break;
            }
        }
        var inputValue = input.value;
        if (inputType !== '' && input.value !== '') {
            if (input.type === 'number') {
                inputValue = parseInt(inputValue,10);
            }
            data[inputType] = inputValue;
        }
    }

    return data;
};

Application.prototype.setRecordFieldDisabled = function (innerForm, status = true) {
    var inputElements = innerForm.getElementsByTagName('input');
    for (var i in inputElements) {
        var inputElement = inputElements[i];
        inputElement.disabled = status === true;
    }
    var buttonElements = innerForm.getElementsByTagName('button');
    for (var i in buttonElements) {
        var buttonElement = buttonElements[i];
        buttonElement.disabled = status === true;
    }
};

Application.prototype.newRecordListener = function () {
    var newRecordSelect = document.querySelector('#newRecordSelector-form select[name=type]');

    var self = this;
    newRecordSelect.addEventListener('change', function (event) {
        event.preventDefault();
        var value = newRecordSelect.value;
        newRecordSelect.value = '';
        if (value === '') {
            return;
        }
        var newRecord = document.getElementById('newRecord');
        if (newRecord.hasChildNodes()) {
            var answer = confirm(
                'You are about to change type of created record to ' + value +
                '. This will erase all filled in values. Are you sure to proceed?'
            );
            if (!answer) {
                return;
            }
            newRecord.innerHTML = '';
        }
        newRecord.appendChild(self.renderRecord(value, [], [], false));
    })
};

Application.prototype.setNewRecordSelectDisabled = function (state) {
    var newRecordSelect = document.querySelector('#newRecordSelector-form select[name=type]');
    newRecordSelect.disabled = state === true;
};
