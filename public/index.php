<?php

use App\DNSDemo\Application\Application;

/** @var Application $application */
$application = require '../config/bootstrap.php';

$application->run();
